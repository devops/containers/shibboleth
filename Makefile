SHELL = /bin/bash

build_tag ?= shibboleth

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) ./src

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	build_tag=$(build_tag) ./test.sh
