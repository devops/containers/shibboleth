#!/bin/bash -e

volume="shibboleth-socket-${CI_COMMIT_SHORT_SHA:-test}"
docker volume create $volume

container="shibboleth-${CI_COMMIT_SHORT_SHA:-test}"
docker run --rm -d -v "${volume}:/var/run/shibboleth" --name $container $build_tag

code=1
interval=5
timeout=60

SECONDS=0
while [[ $SECONDS -lt $timeout ]]; do
    status="$(docker inspect -f '{{.State.Health.Status}}' ${container})"
    echo "Status: $status"

    case $status in
	healthy)
	    code=0
	    break
	    ;;
	unhealthy)
	    break
	    ;;
        starting)
            sleep $interval
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

docker stop $container
docker volume rm $volume

exit $code
